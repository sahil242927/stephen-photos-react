import axios from "axios";

export default axios.create({
  headers: {
    Authorization:
      "Client-ID 1b499e9510d0d3b4ef9ea9e2a0665aab98af6fbac838d0f8613cfaaa83cb973a"
  },
  baseURL: "https://api.unsplash.com/"
});
